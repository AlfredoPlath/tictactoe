﻿using CursoNetCore.Models;
using System.Threading.Tasks;

namespace CursoNetCore.Middlewares
{
    public interface IUserService
    {
        Task<bool> RegisterUser(UserModel userModel);
        Task<UserModel> GetUserByEmail(string email);
        Task UpdateUser(UserModel user);
    }
}