﻿using CursoNetCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CursoNetCore.Middlewares
{
    public class UserService : IUserService
    {
        public Task<UserModel> GetUserByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public Task<bool> RegisterUser(UserModel userModel)
        {
            return Task.FromResult(true);
        }

        public Task UpdateUser(UserModel user)
        {
            throw new NotImplementedException();
        }
    }
}
