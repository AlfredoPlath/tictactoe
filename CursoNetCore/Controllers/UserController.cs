﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CursoNetCore.Middlewares;
using CursoNetCore.Models;
using Microsoft.AspNetCore.Mvc;

namespace CursoNetCore.Controllers
{
    public class UserController : Controller
    {
        private IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(UserModel userModel)
        {
            if (ModelState.IsValid)
            {
                await _userService.RegisterUser(userModel);
                return RedirectToAction(nameof(EmailConfirmation), new { userModel.Email});
            }
            return View(userModel);
        }

        [HttpGet]
        public IActionResult EmailConfirmation(string email)
        {
            ViewBag.Email = email;
            return View();
        }
    }
}