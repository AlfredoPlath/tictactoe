﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CursoNetCore;
using CursoNetCore.Extensions;
using CursoNetCore.Middlewares;
using CursoNetCore.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;

namespace CursoNetCore
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddResponseCompression();
            services.AddSingleton<IUserService, UserService>();
            services.AddRouting();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else {
                app.UseExceptionHandler("/Home/error404");
            }

            var routeBuilder = new RouteBuilder(app);
            /*routeBuilder.MapGet("CreateUser", context => {
                var firstName = context.Request.Query["firstName"];
                var lastName = context.Request.Query["lastName"];
                var email = context.Request.Query["email"];
                var password = context.Request.Query["password"];
                var userService = context.RequestServices.GetService<IUserService>();
            userService.RegisterUser(new UserModel {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                Password = password });
            return context.Response.WriteAsync($"User {firstName} {lastName} has been sucessfully created."); });*/
            var newUserRoutes = routeBuilder.Build();
            var options = new RewriteOptions().AddRewrite("NewUser", "/User/Index", false);


            app.UseStaticFiles();
            app.UseRewriter(options);
            app.UseRouter(newUserRoutes);

            app.UseStaticFiles();
            app.UseResponseCompression();
            app.UseCommunicationMiddleware();

            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "Home",
                    template: "{controller=Home}/{action=Index}"); });

            //app.UseStatusCodePagesWithRedirects("/Home/error{0}");
            //app.UseStatusCodePagesWithReExecute("/Home/Index/");
            app.UseStatusCodePages("text/plain", "HTTP Error - Status Code: {0}");

            //EjemploGeneric<string> e = new EjemploGeneric<string>("", "");
        }
    }
    /*generics, extensors y generic methods, partial classes
    public class Lista<T>
    {

        private ArrayList ListaAttr = new ArrayList();

        public void Add(T t)
        {
            ListaAttr.Add(t);
        }

        public int Count()
        {
            return ListaAttr.Count;
        }

        public T This(int i)
            {
             return (T) ListaAttr[i]; 
            }
    }

    public class Familia {

        private Matrimonio Matrimonio;
        private Lista<Persona> Hijos = new Lista<Persona>();

        public Familia(Matrimonio _matrimonio, params Persona[] _hijos)
        {

            Matrimonio = _matrimonio;
            foreach (Persona p in _hijos)
            {
                Hijos.Add(p);
            }
        }
    }


    public class Matrimonio : ParOrdenado<Persona>
    {

        String Fecha;

        public Matrimonio(Persona marido, Persona mujer, string fecha): base(marido, mujer)
        {

        }

    }

    public class Persona
    {
        private string Nombre, Apellido, Sexo;
        private int Edad;

        public Persona(string _nombre, string _apellido, string _sexo, int _edad)
        {
            Nombre = _nombre;
            Apellido = _apellido;
            Sexo = _sexo;
            Edad = _edad;
        }

    }


    public class ParOrdenado<T> where T: Persona
    {

        private T posicionX = default(T);
        private T posicionY = default(T);

        public ParOrdenado(T euno, T edos)
        {
            posicionX = euno;
            posicionY = edos;

            Procedimiento<int> pint = new Procedimiento<int>(imprimirEntero);
            Procedimiento<string> pstring = new Procedimiento<string>(imprimirString);

            pint(25);
            pstring("hola");
        }

        public void imprimirEntero(int n)
        {

        }

        public void imprimirString(string s)
        {

            ClaseParcial cp = new ClaseParcial();
            cp.MetodoExtensor();
        }

    }

    public delegate void Procedimiento<T>(T t);

    public partial class ClaseParcial {

        partial void hola1();
        partial void hola2();
    }

    public partial class ClaseParcial
    {
        partial void hola2()
        {
            hola1();
        }
    }

}
namespace PlainConcepts.Extensions
{
    public static partial class ClaseParcial_Helper
    {
        public static bool MetodoExtensor(this Persona c)
        {
            return true;
        }

        public static bool MetodoExtensor(this ClaseParcial c)
        {
            return true;
        }

    }*/
}
